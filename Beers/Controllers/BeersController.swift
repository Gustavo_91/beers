//
//  BeersController.swift
//  Beers
//
//  Created by Gustavo Pirela on 29/10/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class BeersController: UIPageViewController {
    // MARK: Properties
    var beers = BeerCollection(beers: [])
    let searchBar = UISearchBar()
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        setPageController(page: .empty)
        setSearchBar()
    }
}

// MARK: - Private
private extension BeersController {
    func setSearchBar() {
        searchBar.delegate = self
        searchBar.scopeButtonTitles = ["Less Alcohol", "More Alcohol"]
        searchBar.showsScopeBar = true
        searchBar.showsCancelButton = true
        searchBar.sizeToFit()
        navigationItem.titleView = searchBar
    }
        
    func setPageController(page: PageType = .content) {
        switch page {
        case .content:
            let viewModel = BeerViewModel(beer: self.beers.current())
            let viewController = BeerViewController(viewModel: viewModel)
            self.setViewControllers([viewController], direction: .forward, animated: false)
        default:
            let viewController = EmptyViewController()
            self.setViewControllers([viewController], direction: .forward, animated: false)
        }
    }
    
    func fetchBeers(withFood food: String) {
        guard var components = URLComponents(string: "https://api.punkapi.com/v2/beers") else { return }
        components.queryItems = [
            URLQueryItem(name: "food", value: food),
            URLQueryItem(name: "per_page", value: String(20))
        ]
        
        guard let url = components.url else { return }
        let request = NetworkRequest(url: url)
        request.execute { data in
            guard let data = data else { return }
            let decoder = JSONDecoder()
            
            guard let beersInventory = try? decoder.decode([Beer].self, from: data),
                beersInventory.count > 0 else {
                    self.beers = BeerCollection(beers: [])
                    DispatchQueue.main.async {
                        self.setPageController(page: .empty)
                    }
                    return
            }
            
            let order = AlcoholVolumeOrder(rawValue: self.searchBar.selectedScopeButtonIndex)
            self.beers = BeerCollection(beers: beersInventory, order: order)
            DispatchQueue.main.async {
                self.setPageController()
            }
        }
    }
    
    enum PageType {
        case empty
        case content
    }
}

// MARK: - UIPageViewControllerDataSource
extension BeersController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let beer = beers.previous() else { return nil }
        let viewModel = BeerViewModel(beer: beer)
        return BeerViewController(viewModel: viewModel)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let beer = beers.next() else { return nil }
        let viewModel = BeerViewModel(beer: beer)
        return BeerViewController(viewModel: viewModel)
    }
    
    func presentationCount(for: UIPageViewController) -> Int {
        return beers.count == 0 ? 1 : beers.count
    }

    func presentationIndex(for: UIPageViewController) -> Int {
        return 0
    }
}

// MARK: - UISearchBarDelegate
extension BeersController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            fetchBeers(withFood: text)
        }
        
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        guard !(viewControllers?[0] is EmptyViewController) else {
            return
        }
        
        switch selectedScope {
        case 0:
            beers.sortBeers(by: .increasing)
        default:
            beers.sortBeers(by: .decreasing)
        }
        
        setPageController()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}


