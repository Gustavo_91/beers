//
//  NetworkRequest.swift
//  Beers
//
//  Created by Gustavo Pirela on 31/10/2019.
//  Copyright © 2019 me. All rights reserved.
//

import Foundation

class NetworkRequest {
    let session = URLSession.shared
    let url: URL
    
    init(url: URL) {
        self.url = url
    }
    
    func execute(withCompletion completion: @escaping (Data?) -> Void) {
        let task = session.dataTask(with: url) {(data, _, _) in
            completion(data)
        }
        
        task.resume()
    }
}
