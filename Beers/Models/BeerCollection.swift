//
//  BeerCollection.swift
//  Beers
//
//  Created by Gustavo Pirela on 30/10/2019.
//  Copyright © 2019 me. All rights reserved.
//

import Foundation

struct BeerCollection {
    private var beers: [Beer]
    private var index = 0
    
    var count: Int {
        return beers.count
    }
    
    func current() -> Beer {
        return beers[index]
    }
    
    mutating func next() -> Beer? {
        guard index < beers.count - 1 else { return nil }
        index += 1
        return beers[index]
    }
    
    mutating func previous() -> Beer? {
        guard index > 0 else { return nil }
        index -= 1
        return beers[index]
    }
    
    mutating func sortBeers(by order: AlcoholVolumeOrder?) {
        switch order {
        case .decreasing:
            beers.sort(by: >)
        default:
            beers.sort(by: <)
        }
        
        index = 0
    }
    
    init(beers: [Beer], order: AlcoholVolumeOrder? = .increasing) {
        self.beers = beers
        sortBeers(by: order)
    }
}

enum AlcoholVolumeOrder: Int {
    case increasing
    case decreasing
}
