//
//  Beer.swift
//  Beers
//
//  Created by Gustavo Pirela on 30/10/2019.
//  Copyright © 2019 me. All rights reserved.
//

import Foundation

struct Beer: Decodable {
    let name: String
    let tagline: String
    let description: String
    let alcoholVolume: Double
    let imageUrl: URL?
    
    enum CodingKeys: String, CodingKey {
        case name
        case tagline
        case description
        case alcoholVolume = "abv"
        case imageUrl = "image_url"
    }
}

extension Beer: Comparable {
    static func < (lhs: Beer, rhs: Beer) -> Bool {
        return lhs.alcoholVolume < rhs.alcoholVolume
    }
    
    static func == (lhs: Beer, rhs: Beer) -> Bool {
        return lhs.alcoholVolume == rhs.alcoholVolume
    }
}
