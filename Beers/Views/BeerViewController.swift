//
//  BeerViewController.swift
//  Beers
//
//  Created by Gustavo Pirela on 30/10/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class BeerViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var tagline: UILabel!
    @IBOutlet weak var beerDescription: UILabel!
    @IBOutlet weak var alcoholVolume: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    let viewModel: BeerViewModel
    

    // MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    init(viewModel: BeerViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Private
    private func setUI() {
        imageView.image = UIImage()
        name.text = viewModel.name
        tagline.text = viewModel.tagline
        beerDescription.text = viewModel.description
        alcoholVolume.text = viewModel.alcoholVolume
        viewModel.setImageFor(imageView: imageView)
    }
}
