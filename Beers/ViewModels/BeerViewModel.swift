//
//  BeerViewModel.swift
//  Beers
//
//  Created by Gustavo Pirela on 30/10/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

struct BeerViewModel {
    private let beer: Beer
    private var image: UIImage?
    
    var name: String {
        return beer.name
    }
    
    var tagline: String {
        return beer.tagline
    }
    
    var description: String {
        return beer.description
    }
    
    var alcoholVolume: String {
        return String(beer.alcoholVolume) + "%"
    }
        
    init(beer: Beer) {
        self.beer = beer
    }
    
    func setImageFor(imageView: UIImageView) {
        guard let url = beer.imageUrl else {
            imageView.image = UIImage(named: "beer")
            return
        }
        
        let request = NetworkRequest(url: url)
        request.execute {data in
            guard let data = data, let image = UIImage(data: data) else { return }
            DispatchQueue.main.async {
                imageView.image = image
            }
        }
    }
    
}
