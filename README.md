# Project Title

Beers. Match the best beers for you meals

## Getting Started

1. Open Xcode
2. Build and Run the **Beers** target
3. The first time you open it, a white screen will appear for a brief period of time. That is the window view because I didn't put any launch screen image

### Prerequisites

* Xcode 11
* Swift 5.1

## Author

* **Gustavo Pirela**


